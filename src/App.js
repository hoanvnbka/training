/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import UserScreen from './react_navigation/screens/UserScreen';
import DetailScreen from './react_navigation/screens/DetailScreen';
import LoginScreen from './react_navigation/screens/LoginScreen';
import RegisterScreen from './react_navigation/screens/RegisterScreen';
import UserDetailScreen from './react_navigation/screens/UserDetailScreen';
import PostScreen from './react_navigation/screens/PostScreen';
import PostDetailScreen from './react_navigation/screens/PostDetailScreen';
import {createAppContainer, createStackNavigator, createBottomTabNavigator} from 'react-navigation';


const UserNavigator = createStackNavigator({
  User: {
    screen: UserScreen
  },
  Detail: {
    screen: DetailScreen
  },
  Login: {
    screen: LoginScreen
  },
  Register: {
    screen: RegisterScreen
  },
  UserDetail: {
    screen: UserDetailScreen
  }
}, {
  initialRouteName: 'User',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f4511e'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }
});

const PostNavigator = createStackNavigator({
  Post: {
    screen: PostScreen
  },
  PostDetail: {
    screen: PostDetailScreen
  }
}, {
  initialRouteName: 'Post',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f4511e'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }
});

const AppNavigator = createBottomTabNavigator({
  User: {
    screen: UserNavigator
  },
  Post: {
    screen: PostNavigator
  }
});

const AppContainer = createAppContainer(AppNavigator);

type Props = {};
export default class App extends Component<Props> {
  render() {
    return <AppContainer />;
  }
}
