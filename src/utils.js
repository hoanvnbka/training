import {AsyncStorage} from 'react-native';


export const USER_KEY = 'auth-key';

export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");

export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const isSignIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER_KEY)
        .then(res => {
            if (res !== null) {
                resolve(true);
            } else {
                reject(false);
            }
        })
        .catch(error => reject(error));
    });
}
