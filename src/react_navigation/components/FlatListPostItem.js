/**
 * @flow
 */


import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';


export default class FlatListPostItem extends Component {
    render() {
        return (
            <View style={{flex: 1, alignContent: 'flex-start', alignItems: 'flex-start'}}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('PostDetail', {
                        userId: this.props.userId,
                        body: this.props.body
                    })}
                >
                    <Text style={{color: 'red'}}>{this.props.id} - {this.props.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
