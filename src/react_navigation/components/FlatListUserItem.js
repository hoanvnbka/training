import React, {Component} from 'react';
import {View, Button} from 'react-native';


export default class FlatListUserItem extends Component {
    render() {
        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <Button
                    title={this.props.username}
                    onPress={() => this.props.navigation.navigate('UserDetail', {
                        name: this.props.name,
                        email: this.props.email,
                        // address: this.props.address,
                        phone: this.props.phone,
                        website: this.props.website,
                        // company: this.props.company
                    })}
                />
            </View>
        );
    } 
}