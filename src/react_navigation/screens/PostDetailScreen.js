/**
 * @flow
 */


import React, {Component} from 'react';
import {Text, View} from 'react-native';


export default class PostDetailScreen extends Component {
    render() {
        const {navigation} = this.props;
        const userId: number = navigation.getParam('userId', '');
        const body: string = navigation.getParam('body', '');

        return (
            <View style={{flex: 1, alignContent: 'flex-start', alignItems: 'flex-start'}}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>User Id: {userId}</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Body: {body}</Text>
            </View>
        );
    }
}
