/**
 * @flow
 */


import React, {Component} from 'react';
import {View, ActivityIndicator, FlatList} from 'react-native';
import FlatListPostItem from '../components/FlatListPostItem';


export default class PostScreen extends Component {
    static navigationOptions = {
        title: 'Posts'
    };

    state: {isLoading: boolean} = {
        isLoading: true
    };

    async componentDidMount() {
        try {
            const res = await fetch('https://jsonplaceholder.typicode.com/posts');
            const response = await res.json();

            this.setState({
                isLoading: false,
                posts: response
            });
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator />
                </View>
            );
        }
        
        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <FlatList
                    data={this.state.posts}
                    renderItem={({item}) => {
                        return (
                            <FlatListPostItem
                                userId={item.userId}
                                id={item.id}
                                title={item.title}
                                body={item.body}
                                navigation={this.props.navigation}
                            />
                        );
                    }}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }
}