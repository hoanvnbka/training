import React, {Component} from 'react';
import {Text, View} from 'react-native';


export default class UserDetailScreen extends Component {
    static navigationOptions = {
        title: 'UserDetail'
    };

    render() {
        const {navigation} = this.props;
        const name = navigation.getParam('name', '');
        const email = navigation.getParam('email', '');
        const phone = navigation.getParam('phone', '');
        const website = navigation.getParam('website', '');
        const address = navigation.getParam('address', '');
        const company = navigation.getParam('company', '');

        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'flex-start'}}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Name: {name}</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Email: {email}</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Phone: {phone}</Text>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Website: {website}</Text>
                {/* <Text style={{fontSize: 10, fontWeight: 'bold'}}>Address: {address}</Text>
                <Text style={{fontSize: 10, fontWeight: 'bold'}}>Company: {company}</Text> */}
            </View>
        );
    }
}