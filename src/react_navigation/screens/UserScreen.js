import React, {Component} from 'react';
import {ActivityIndicator, FlatList, Image, View, Text, Button} from 'react-native';
import FlatListUserItem from '../components/FlatListUserItem';


export default class UserScreen extends Component {
    state = {
        isLoading: true
    };

    // Setting header screen
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: <Image style={{width: 50, height: 50}} source={require('../images/spiro.png')} />
        };
    };

    async componentDidMount() {
        try {
            const res = await fetch('https://jsonplaceholder.typicode.com/users');
            const response = await res.json();
            this.setState({
                isLoading: false,
                users: response
            });
        } catch (error) {
            return console.error(error);
        }
    }

    render() {
        if(this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
                <Text style={{fontSize: 30, fontWeight: 'bold', margin: 10}}>Home Screen</Text>
                <FlatList
                    data={this.state.users}
                    renderItem={({item}) => {
                        return (
                            <FlatListUserItem
                                name={item.name}
                                username={item.username}
                                email={item.email}
                                address={item.address}
                                phone={item.phone}
                                website={item.website}
                                company={item.company}
                                navigation={this.props.navigation}
                            />
                        );
                    }}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }
}
