/**
 * flow
 */


import React, {Component} from 'react';
import {TextInput, View, Text, Button} from 'react-native';


export default class RegisterScreen extends Component {
    static navigationOptions = {
        title: 'Register'
    };

    state: {
        email: string,
        password: string,
        rePassword: string
    } = {
        email: '',
        password: '',
        rePassword: ''
    };

    onChangeEmailRes = (email: string) => {
        this.setState({email: email});
    };

    onChangePasswordRes = (password: string) => {
        this.setState({password: password});
    };

    onChangeRePasswordRes = (rePassword: string) => {
        this.setState({rePassword: rePassword});
    };

    onRegister = () => {
        if (this.state.email !== undefined && this.state.email !== '' &&
            this.state.password !== undefined && this.state.password !== '' &&
            this.state.rePassword !== undefined && this.state.rePassword !== '' &&
            this.state.password === this.state.rePassword
        ) {
            alert('Success Register');
            this.props.navigation.navigate('Login', {
                email: this.state.email,
                password: this.state.password
            });
        } else {
            alert('There was an error!');
        }
    }

    render() {
        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 30, fontWeight: 'bold'}}>Register</Text>
                <TextInput
                    placeholder="email"
                    value={this.state.email}
                    onChangeText={email => this.onChangeEmailRes(email)}
                />
                <TextInput
                    placeholder="password"
                    value={this.state.password}
                    onChangeText={password => this.onChangePasswordRes(password)}
                    secureTextEntry={true}
                />
                <TextInput
                    placeholder="Re Password"
                    value={this.state.rePassword}
                    onChangeText={rePassword => this.onChangeRePasswordRes(rePassword)}
                    secureTextEntry={true}
                />
                <Button
                    title="Register"
                    onPress={this.onRegister}
                />
            </View>
        );
    }
}
