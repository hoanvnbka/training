import React, {Component} from 'react';
import {Button, View, Text} from 'react-native';


export default class DetailScreen extends Component {
    static navigationOptions = ({navigation, navigationOptions}) => {
        const {params} = navigation.state;
        return {
            title: params ? params.title : 'Default title',
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor
        };   
    };

    render() {
        const {navigation} = this.props;
        const id = navigation.getParam('id', 'default id');
        const content = navigation.getParam('content', 'default content');

        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <Text>Detail Screen</Text>
                <Text>Id: {JSON.stringify(id)}</Text>
                <Text>Content: {JSON.stringify(content)}</Text>
                <Button
                    title="Go to Detail Again"
                    onPress={() => this.props.navigation.push('Detail', {
                        id: Math.floor(Math.random() * 100)
                    })}
                />

                <Button
                    title="Go to Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />

                <Button
                    title="Go back"
                    onPress={() => this.props.navigation.goBack()}
                />
            </View>
        );
    }
}
