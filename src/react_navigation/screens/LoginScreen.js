/**
 * @flow
 */


import React, {Component} from 'react';
import {TextInput, View, Text, Button} from 'react-native';


export default class LoginScreen extends Component {
    static navigationOptions = {
        title: 'Login'
    };

    state: {
        email: string,
        password: string
    } = {
        email: '',
        password: ''
    };

    onChangeEmail = (email: string) => {
        this.setState({
            email: email
        })
    };

    onChangePassword = (password: string) => {
        this.setState({
            password: password
        });
    };

    onLogin = (email, password) => {
        if (email === 'admin@gmail.com' && password === 'admin') {
            alert('Success Login');
            this.props.navigation.navigate('Home');
        } else {
            alert('Error Email or Password');
        }
    }

    render() {
        const {navigation} = this.props;
        const email = navigation.getParam('email', '');
        const password = navigation.getParam('password', '');

        return (
            <View style={{flex: 1}}>
                <Text>Login</Text>
                <TextInput
                    value={this.state.email}
                    placeholder="Email"
                    onChangeText={email => {
                        this.onChangeEmail(email)}
                    }
                />
                <Text>Password</Text>
                <TextInput
                    value={this.state.password}
                    placeholder="Password"
                    onChangeText={password => this.onChangePassword(password)}
                    secureTextEntry={true}
                />
                <Button
                    title="Login"
                    onPress={() => this.onLogin(this.state.email, this.state.password)}
                />
                <Text>You haven't Account?</Text>
                <Button
                    title="Register"
                    onPress={() => this.props.navigation.navigate('Register')}
                />
            </View>
        );
    }
}
